# Dokumentation für Standards im Programm DigitalFirst
Standards werden zu unterschiedlichen Phasen eines Projektes und für unterschiedliche Aufgaben genutzt. Dieser Bereich beschreibt die aktuellen Vorgaben im Bereich der Standardisierung.

## Anforderungen
- EFA und NON-EFA Unterscheidung. Was macht ein EFA-OD aus?
  Umfang, Daten/FIM, Komponentenbildung, Handlungsanweisungen.
- Klare Vorgaben für Nachnutzung: was benötigen wir?
- Parametrisierung im PVOG
  1) Leistung und Behörde finden
  2) Individualisierung
  3) Routing für Transport
- Welche Daten gehören nicht ins PVOG? 

## Konzeption
- Datenmodelle
- Service Design: WireFrame, HD-Prototypen
- Anbindung, Transport über FIM oder XÖV
- Sicherheitsbedarf

## Entwicklung
- Entwurfsmuster für Code und Komponenten.
  Wiederverwertbare Komponenten.
  Beispielcode für die Integration von Komponenten (Darko / Core Team).
  Empfehlungen zur Vorgehensweise um Kosten zu senken und Wartbarkeit zu verbessen.

- Beispiel
    Uploadkomponente fehlerhaft.
    --> Eine neue Uploadkomponente hat ein neues Deployment aller ODs zur Folge.
    Daraus Folgeprobleme durch Abhängigkeiten zwischen Bibliotheken.
    Hohe Zahl an "technischen Schulden" zur Folge, die bereinigt werden müssen.
    Dadurch hoher Testaufwand, weil viele Bestandteile eines ODs geändert wurden.

- Wiederverwendbare AFM Komponenten, Code, Services

- Ausblick auf OD-Ökosystem:
  -- welche Services sollten zentral und übergreifend angeboten werden?
  -- Verteilte Systeme: Auswirkungen von Schnittstellen und Abhängigkeiten!

- Handlungsanweisungen zur Verwendung von Standards!

### Punkte
- Wrapper
- Validierung
- FIM
- Verlagerung von Features aus dem OD heraus in zentrale Services


## Sonstiges
- Abstimmung mit den anderen DP-Trägerländern
- Themen besprechen und planen
- Gibt es noch weitere Punkte?
- Regelmäßiger Austausch auf technischer und fachlicher Ebene mit den DP-Ländern.