# Wir unterstützen XUnternehmen

Unsere EFA-Onlinedienste benutzen Fachmodule in XUnternehmen.
Diese sind auf dem [XRepository](https://xrepository.de) dokumentiert.

Kontakt:
[Axel Wolters](mailto:axel.wolters@sk.hamburg.de)

## Übersicht über erstellte Fachmodule

* [XUnternehmen.Arbeitszeit](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:arbeitszeit)
* [XUnternehmen.Pyrotechnik](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:pyrotechnik)
* [XUnternehmen.Personalanzeigen.SprengG](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:personalanzeigen.sprengg)
* [XUnternehmen.Personalanzeigen.Heimarbeit](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:personalanzeigen.heimarbeit)
* [XUnternehmen.Personalanzeigen.Kita](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:personalanzeigen.kita)
* [XUnternehmen.Personalanzeigen.ArzneimittelG](XUnternehmen.Personalanzeigen.ArzneimittelG)
* [XUnternehmen.Kündigungsverbote](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:kuendigungsverbote)
* [XUnternehmen.Kündigungsverbote.Schwerbehinderte](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:kuendigungsverbote.schwerbehinderte)
* [XUnternehmen.MitwirkungKinder](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:mitwirkungkinder)
* [XUnternehmen.BegleitendeHilfeArbeitgeber](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:begleitendehilfearbeitgeber)
* [XUnternehmen.SondernutzungStraßen](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:sondernutzungstrassen)
* [XUnternehmen.WochenSpezialmärkte](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:wochenspezialmaerkte)
* [XUnternehmen.Mutterschutz](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:mutterschutz)
