# Befreiung von der Ausweispflicht

**Title:** Befreiung von der Ausweispflicht

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="handelndePerson"></a>1. [Required] Property Befreiung von der Ausweispflicht > handelndePerson</strong>  

</summary>
<blockquote>

**Title:** Handelnde Person (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/handelndePerson                                  |

**Description:** basiert auf G1700742. Alle Pflichtfelder wurden durch Bund.ID verifiziert.

<details>
<summary>
<strong> <a name="handelndePerson_istAntragstellendePerson"></a>1.1. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > istAntragstellendePerson</strong>  

</summary>
<blockquote>

**Title:** Ist die handelnde Person = Antragsteller? 

|                |                                        |
| -------------- | -------------------------------------- |
| **Type**       | `enum (of string)`                     |
| **Defined in** | #/definitions/istAntragstellendePerson |

**Description:** Basiert auf F17012146 der Type wurde angepasst. Die Values sind strings.

Must be one of:
* "ja"
* "nein"

**Examples:**

```json
"ja"
```

```json
"nein"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_familiennamen"></a>1.2. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > familiennamen</strong>  

</summary>
<blockquote>

**Title:** Familienname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/familienname |

**Description:** Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.
Basierend auf F60000227.

| Restrictions   |     |
| -------------- | --- |
| **Min length** | 1   |
| **Max length** | 120 |

**Example:**

```json
"Mustermann"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_vornamen"></a>1.3. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > vornamen</strong>  

</summary>
<blockquote>

**Title:** Vornamen

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/vornamen |

**Description:** Laut BSI TR-03123 soll Vorname <= 80 Zeichen betragen.
Laut PAuswV soll Vorname nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen. Basierend auf F60000228

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 80 |

**Example:**

```json
"Claudia Melanie"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift"></a>1.4. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift</strong>  

</summary>
<blockquote>

**Title:** Anschrift Inland oder Ausland mit Frage

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `combining`                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/anschrift                                        |

**Description:** Zu beachten: wird die Anschrift im Kontext der Online-Ausweisfunktion des neuen Personalausweis verwendet, können nur Personen mit Anschriften in Deutschland authentisiert werden (siehe auch https://www.personalausweisportal.de/DE/Buergerinnen-und-Buerger/Online-Ausweisen/Wohnsitz_im_Ausland/wohnsitz_im_ausland_node.html). Bei der Umsetzung müssen somit ausländische Anschriften ausgeschlossen werden.

<blockquote>

| One of(Option)                                                           |
| ------------------------------------------------------------------------ |
| [Anschrift Inland Straßenanschrift](#handelndePerson_anschrift_oneOf_i0) |
| [item 1](#handelndePerson_anschrift_oneOf_i1)                            |

<blockquote>

#### <a name="handelndePerson_anschrift_oneOf_i0"></a>1.4.1. Property `Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift`

**Title:** Anschrift Inland Straßenanschrift

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/anschriftInlandStrasse                           |

**Description:** Basierend auf G60000086. Mit der Abweichung, dass Strasse und Hausnummer zusammengefasst wurden, damit die Kompatiblilität mit den Nutzerkonten gewahrt bleibt.

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_staat"></a>1.4.1.1. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > staat</strong>  

</summary>
<blockquote>

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"Deutschland"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_strasseHausnummer"></a>1.4.1.2. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > strasseHausnummer</strong>  

</summary>
<blockquote>

**Title:** Straße

|                        |                                                                   |
| ---------------------- | ----------------------------------------------------------------- |
| **Type**               | `string`                                                          |
| **Same definition as** | [strasseHausnummer](#handelndePerson_anschrift_strasseHausnummer) |

**Description:** Basierend auf F60000243 kombiniert aber F60000243 mit Hausnummer F60000244, um Kompatibilität mit den Nutzkontne zu erhalten. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 50. Bei XInneres 8 ist die Feldlänge <= 55 Zeichen.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_plz"></a>1.4.1.3. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > plz</strong>  

</summary>
<blockquote>

**Title:** Postleitzahl

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [plz](#handelndePerson_anschrift_plz) |

**Description:** Basierend auf F60000246

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_ort"></a>1.4.1.4. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > ort</strong>  

</summary>
<blockquote>

**Title:** Ort

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [ort](#handelndePerson_anschrift_ort) |

**Description:** Basierend auf F60000247. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.
Laut BSI TR-03123 kleiner gleich 105 Zeichen.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_adresszusatz"></a>1.4.1.5. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > adresszusatz</strong>  

</summary>
<blockquote>

**Title:** Anschrift Zusatzangaben

|                        |                                                         |
| ---------------------- | ------------------------------------------------------- |
| **Type**               | `string`                                                |
| **Same definition as** | [adresszusatz](#handelndePerson_anschrift_adresszusatz) |

**Description:** Basierend auf F60000248.

</blockquote>
</details>

</blockquote>
<blockquote>

#### <a name="handelndePerson_anschrift_oneOf_i1"></a>1.4.2. Property `Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > item 1`

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i1_staat"></a>1.4.2.1. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > item 1 > staat</strong>  

</summary>
<blockquote>

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `combining`                                                                 |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |

###### <a name="autogenerated_heading_2"></a>1.4.2.1.1. Must **not** be

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"Deutschland"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i1_anschriftKomplett"></a>1.4.2.2. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > oneOf > item 1 > anschriftKomplett</strong>  

</summary>
<blockquote>

**Title:** Internationale Anschrift

|                        |                                                                   |
| ---------------------- | ----------------------------------------------------------------- |
| **Type**               | `string`                                                          |
| **Same definition as** | [anschriftKomplett](#handelndePerson_anschrift_anschriftKomplett) |

**Description:** Internationaler Adressblock in Abweichung von G60000091 zur kompatibiliät mit den Nutzerkonten

</blockquote>
</details>

</blockquote>

</blockquote>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_staat"></a>1.4.3. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > staat</strong>  

</summary>
<blockquote>

**Title:** Staat

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/staat |

**Description:** Entspricht dem FIM-Baustein F60000261. Die Werte entsprechen der amtlichen Kurzform des Staatsnamen aus der Codeliste (urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staat)

**Examples:**

```json
"Deutschland"
```

```json
"Frankreich"
```

```json
"Polen"
```

```json
"Vereinigtes Königreich"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_strasseHausnummer"></a>1.4.4. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > strasseHausnummer</strong>  

</summary>
<blockquote>

**Title:** Straße

|                |                                 |
| -------------- | ------------------------------- |
| **Type**       | `string`                        |
| **Defined in** | #/definitions/strasseHausnummer |

**Description:** Basierend auf F60000243 kombiniert aber F60000243 mit Hausnummer F60000244, um Kompatibilität mit den Nutzkontne zu erhalten. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 50. Bei XInneres 8 ist die Feldlänge <= 55 Zeichen.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 66 |

**Examples:**

```json
"Musterstr. 12a"
```

```json
"E 4, 6"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_plz"></a>1.4.5. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > plz</strong>  

</summary>
<blockquote>

**Title:** Postleitzahl

|                |                   |
| -------------- | ----------------- |
| **Type**       | `string`          |
| **Defined in** | #/definitions/plz |

**Description:** Basierend auf F60000246

| Restrictions                      |                                                                                                                                                                                                       |
| --------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 5                                                                                                                                                                                                     |
| **Max length**                    | 5                                                                                                                                                                                                     |
| **Must match regular expression** | ```^([0]{1}[1-9]{1}\|[1-9]{1}[0-9]{1})[0-9]{3}$``` [Test](https://regex101.com/?regex=%5E%28%5B0%5D%7B1%7D%5B1-9%5D%7B1%7D%7C%5B1-9%5D%7B1%7D%5B0-9%5D%7B1%7D%29%5B0-9%5D%7B3%7D%24&testString=10999) |

**Example:**

```json
10999
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_ort"></a>1.4.6. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > ort</strong>  

</summary>
<blockquote>

**Title:** Ort

|                |                   |
| -------------- | ----------------- |
| **Type**       | `string`          |
| **Defined in** | #/definitions/ort |

**Description:** Basierend auf F60000247. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.
Laut BSI TR-03123 kleiner gleich 105 Zeichen.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 50 |

**Examples:**

```json
"Berlin"
```

```json
"Hamburg"
```

```json
"Lüneburg"
```

```json
"Mannheim"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_adresszusatz"></a>1.4.7. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > adresszusatz</strong>  

</summary>
<blockquote>

**Title:** Anschrift Zusatzangaben

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/adressZusatz |

**Description:** Basierend auf F60000248.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 21 |

**Example:**

```json
"Hinterhaus rechts"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_anschriftKomplett"></a>1.4.8. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > anschrift > anschriftKomplett</strong>  

</summary>
<blockquote>

**Title:** Internationale Anschrift

|                |                                 |
| -------------- | ------------------------------- |
| **Type**       | `string`                        |
| **Defined in** | #/definitions/anschriftKomplett |

**Description:** Internationaler Adressblock in Abweichung von G60000091 zur kompatibiliät mit den Nutzerkonten

**Example:**

```json
"ul. Grodzka 20/ 6 70-560 Szczecin"
```

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation"></a>1.5. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > kommunikation</strong>  

</summary>
<blockquote>

**Title:** Kommunikation (wenig, ohne De-Mail)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/kommunikation                                    |

**Description:** G60000183. In dieser Datenfeldgruppe wird per Regel gefordert, dass mindestens eine Kommunikationsmöglichkeit angegeben werden muss. Falls es gewünscht ist, dass keine Kommunikationsmöglichkeit angegeben werden braucht, muss die ganze Feldgruppe bei der Verwendung optional gesetzt werden.

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_telefon"></a>1.5.1. [Required] Property Befreiung von der Ausweispflicht > handelndePerson > kommunikation > telefon</strong>  

</summary>
<blockquote>

**Title:** Telefon

|                |                       |
| -------------- | --------------------- |
| **Type**       | `string`              |
| **Defined in** | #/definitions/telefon |

**Description:** Basierend auf F60000240. Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 23 |

**Examples:**

```json
"+491234342567"
```

```json
"01718765463"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_telefax"></a>1.5.2. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > kommunikation > telefax</strong>  

</summary>
<blockquote>

**Title:** Telefax

|                |                       |
| -------------- | --------------------- |
| **Type**       | `string`              |
| **Defined in** | #/definitions/telefax |

**Description:** Basierened auf F60000241. Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 23 |

**Examples:**

```json
"+4917112345678"
```

```json
"00493012345678"
```

```json
"030123456789"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_email"></a>1.5.3. [Optional] Property Befreiung von der Ausweispflicht > handelndePerson > kommunikation > email</strong>  

</summary>
<blockquote>

**Title:** E-Mail

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/email |

**Description:** Basiert auf F60000242

| Restrictions                      |                                                                                                                                                                                                                      |
| --------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 6                                                                                                                                                                                                                    |
| **Max length**                    | 254                                                                                                                                                                                                                  |
| **Must match regular expression** | ```^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$``` [Test](https://regex101.com/?regex=%5E%5BA-Za-z0-9._%25%2B-%5D%2B%40%5BA-Za-z0-9.-%5D%2B%5C.%5BA-Za-z%5D%7B1%2C63%7D%24&testString=%22example%40test.de%22) |

**Examples:**

```json
"example@test.de"
```

```json
"beispiel@demo.com"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson"></a>2. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson</strong>  

</summary>
<blockquote>

**Title:** Antragsstellende Person (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/antragstellendePerson                            |

**Description:** Basiert auf G17007419

<details>
<summary>
<strong> <a name="antragstellendePerson_vorname"></a>2.1. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson > vorname</strong>  

</summary>
<blockquote>

**Title:** Vornamen

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [vornamen](#handelndePerson_vornamen) |

**Description:** Laut BSI TR-03123 soll Vorname <= 80 Zeichen betragen.
Laut PAuswV soll Vorname nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen. Basierend auf F60000228

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_familienname"></a>2.2. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson > familienname</strong>  

</summary>
<blockquote>

**Title:** Familienname

|                        |                                                 |
| ---------------------- | ----------------------------------------------- |
| **Type**               | `string`                                        |
| **Same definition as** | [familiennamen](#handelndePerson_familiennamen) |

**Description:** Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.
Basierend auf F60000227.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_geburtsdatum"></a>2.3. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson > geburtsdatum</strong>  

</summary>
<blockquote>

**Title:** Geburtsdatum

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/geburtsdatum |

**Description:** Entspricht der Übermittlung des Geburtsdatum aus den Nutzerkonten. Es dürfen Tag oder Tag und Monat unbekannt sein. Sie werden dann mit Nullen aufgefüllt.

| Restrictions                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **Must match regular expression** | ```^(19\|2[0-1])\d{2}-((0[13578]\|1[02])-31\|(0[1,3-9]\|1[0-2])-(29\|30))\|((19\|2[0-1])(0[48]\|[2468][048]\|[13579][26]))-02-29\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-8])\|2000-02-29\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-00\|(19\|2[0-1])\d{2}-00-00\|0000-00-00\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-XX\|(19\|2[0-1])\d{2}-XX-XX\|XXXX-XX-XX$``` [Test](https://regex101.com/?regex=%5E%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%28%280%5B13578%5D%7C1%5B02%5D%29-31%7C%280%5B1%2C3-9%5D%7C1%5B0-2%5D%29-%2829%7C30%29%29%7C%28%2819%7C2%5B0-1%5D%29%280%5B48%5D%7C%5B2468%5D%5B048%5D%7C%5B13579%5D%5B26%5D%29%29-02-29%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-%280%5B1-9%5D%7C1%5Cd%7C2%5B0-8%5D%29%7C2000-02-29%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-00%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-00-00%7C0000-00-00%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-XX%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-XX-XX%7CXXXX-XX-XX%24&testString=%222010-00-00%22) |

**Examples:**

```json
"2010-00-00"
```

```json
"2010-XX-XX"
```

```json
"2010-01-00"
```

```json
"2010-01-XX"
```

```json
"2010-01-31"
```

```json
"2010-01-31"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_geburtsort"></a>2.4. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson > geburtsort</strong>  

</summary>
<blockquote>

**Title:** Geburtsort

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/geburtsort |

**Description:** Basiert auf F60000234. Laut BSI TR-03123 soll Geburtsort <= 80 Zeichen betragen. Laut PAuswV soll Geburtsort nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 80 |

**Examples:**

```json
"München"
```

```json
"Berlin"
```

```json
"Lyon"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_anschrift"></a>2.5. [Required] Property Befreiung von der Ausweispflicht > antragstellendePerson > anschrift</strong>  

</summary>
<blockquote>

**Title:** Anschrift Inland oder Ausland mit Frage

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `combining`                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Same definition as**    | [anschrift](#handelndePerson_anschrift)                        |

**Description:** Zu beachten: wird die Anschrift im Kontext der Online-Ausweisfunktion des neuen Personalausweis verwendet, können nur Personen mit Anschriften in Deutschland authentisiert werden (siehe auch https://www.personalausweisportal.de/DE/Buergerinnen-und-Buerger/Online-Ausweisen/Wohnsitz_im_Ausland/wohnsitz_im_ausland_node.html). Bei der Umsetzung müssen somit ausländische Anschriften ausgeschlossen werden.

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung"></a>3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung</strong>  

</summary>
<blockquote>

**Title:** Basierend auf G17007420. Ermessungsgrundlage Befreiung (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `combining`                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/ermessensgrundlageBefreiung                      |

<blockquote>

| One of(Option)                                  |
| ----------------------------------------------- |
| [item 0](#ermessensgrundlageBefreiung_oneOf_i0) |
| [item 1](#ermessensgrundlageBefreiung_oneOf_i1) |
| [item 2](#ermessensgrundlageBefreiung_oneOf_i2) |

<blockquote>

### <a name="ermessensgrundlageBefreiung_oneOf_i0"></a>3.1. Property `Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 0`

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i0_begruendung"></a>3.1.1. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 0 > begruendung</strong>  

</summary>
<blockquote>

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"001"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i0_handlungsunfaehigkeit"></a>3.1.2. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 0 > handlungsunfaehigkeit</strong>  

</summary>
<blockquote>

**Title:** Handlungsunfähigkeit nachweisen (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red)              |
| **Same definition as**    | [handlungsunfaehigkeit](#ermessensgrundlageBefreiung_handlungsunfaehigkeit) |

**Description:** Basiert auf G17007421. Enthält die Nachweise die für LeiKa-Schlüssel 99008002010003 notwendig sind. Und auf PAuswG §1 Abs.3 Nr. 1 abziehlen.

</blockquote>
</details>

</blockquote>
<blockquote>

### <a name="ermessensgrundlageBefreiung_oneOf_i1"></a>3.2. Property `Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 1`

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i1_begruendung"></a>3.2.1. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 1 > begruendung</strong>  

</summary>
<blockquote>

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"002"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i1_unterbringung"></a>3.2.2. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 1 > unterbringung</strong>  

</summary>
<blockquote>

**Title:** Dauerhaft in Pfege-/Krankenhaus untergebracht nachweisen (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Same definition as**    | [unterbringung](#ermessensgrundlageBefreiung_unterbringung)    |

**Description:** Basiert auf G17007423. Enthält die Nachweise die für LeiKa-Schlüssel 99008002010002 notwendig sind. Und auf PAuswG §1 Abs.3 Nr. 2 abziehlen.

</blockquote>
</details>

</blockquote>
<blockquote>

### <a name="ermessensgrundlageBefreiung_oneOf_i2"></a>3.3. Property `Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 2`

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i2_begruendung"></a>3.3.1. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 2 > begruendung</strong>  

</summary>
<blockquote>

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"003"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_oneOf_i2_dauerhafteBehinderung"></a>3.3.2. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > oneOf > item 2 > dauerhafteBehinderung</strong>  

</summary>
<blockquote>

**Title:** Nachweis einer dauerhaften Behinderung (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** Basiert auf G17007423. Enthält die Nachweise die für LeiKa-Schlüssel 99008002010003 notwendig sind. Und auf PAuswG §1 Abs.3 Nr. 3 abziehlen.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_documentId"></a>3.3.2.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_originalName"></a>3.3.2.2. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_size"></a>3.3.2.3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_type"></a>3.3.2.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>

</blockquote>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_begruendung"></a>3.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > begruendung</strong>  

</summary>
<blockquote>

**Title:** Ermessungsgrundlage Befreiung auswählen (PAuswG)

|                |                           |
| -------------- | ------------------------- |
| **Type**       | `enum (of string)`        |
| **Defined in** | #/definitions/begruendung |

**Description:** Basiert auf F17012136
Grundlage nach Personalausweisgesetz §1 Abs. 3.
001 entspricht Leika-Schlüssel 99008002010001 betreute Personen
002 entspricht Leika-Schlüssel 99008002010002 dauerhaft untergebrachte Personen
003 entspricht Leika-Schlüssel 99008002010003 dauerhaft behinderte Personen, die sich nicht allein in der Öffentlichkeit bewegen können 

Must be one of:
* "001"
* "002"
* "003"

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit"></a>3.5. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit</strong>  

</summary>
<blockquote>

**Title:** Handlungsunfähigkeit nachweisen (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/handlungsunfaehigkeit                            |

**Description:** Basiert auf G17007421. Enthält die Nachweise die für LeiKa-Schlüssel 99008002010003 notwendig sind. Und auf PAuswG §1 Abs.3 Nr. 1 abziehlen.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer"></a>3.5.1. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer</strong>  

</summary>
<blockquote>

**Title:** Betreuerausweis hochladen (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** basiert auf F17012137 wird aber als File-Referenz und nicht als Base64 umgesetzt.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_documentId"></a>3.5.1.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_originalName"></a>3.5.1.2. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_size"></a>3.5.1.3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_type"></a>3.5.1.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_vorsorgevollmacht"></a>3.5.2. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > vorsorgevollmacht</strong>  

</summary>
<blockquote>

**Title:** Vorsorgevollmacht hochladen (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** basiert auf F17012138 wird aber als File-Referenz und nicht als Base64 umgesetzt

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_documentId"></a>3.5.2.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_originalName"></a>3.5.2.2. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_size"></a>3.5.2.3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_type"></a>3.5.2.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_unterbringung"></a>3.6. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > unterbringung</strong>  

</summary>
<blockquote>

**Title:** Dauerhaft in Pfege-/Krankenhaus untergebracht nachweisen (PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/unterbringung                                    |

**Description:** Basiert auf G17007423. Enthält die Nachweise die für LeiKa-Schlüssel 99008002010002 notwendig sind. Und auf PAuswG §1 Abs.3 Nr. 2 abziehlen.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_unterbringung_annerkanntesPflegeheim"></a>3.6.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > unterbringung > annerkanntesPflegeheim</strong>  

</summary>
<blockquote>

**Title:** Anerkanntes Pflegeheim? (PAuswG)

|                |                                      |
| -------------- | ------------------------------------ |
| **Type**       | `enum (of string)`                   |
| **Defined in** | #/definitions/annerkanntesPflegeheim |

**Description:** basiert auf F17012140

Must be one of:
* "ja"
* "nein"

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_unterbringung_aufnahmebestaetigung"></a>3.6.2. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > unterbringung > aufnahmebestaetigung</strong>  

</summary>
<blockquote>

**Title:** Aufnahmebestätigung Krankenhaus oder Pflegeheim hochladen (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** basiert auf F17012139 wird aber als File-Referenz und nicht als Base64 umgesetzt.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_documentId"></a>3.6.2.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_originalName"></a>3.6.2.2. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_size"></a>3.6.2.3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_type"></a>3.6.2.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_unterbringung_nachweisPflegestufe"></a>3.6.3. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > unterbringung > nachweisPflegestufe</strong>  

</summary>
<blockquote>

**Title:** Pflegestufe Nachweis hochladen (PAuswG)

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** basiert auf F17012139 wird aber als File-Referenz und nicht als Base64 umgesetzt.

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_documentId"></a>3.6.3.1. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_originalName"></a>3.6.3.2. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_size"></a>3.6.3.3. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer_type"></a>3.6.3.4. [Required] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > handlungsunfaehigkeit > betreuer > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="ermessensgrundlageBefreiung_dauerhafteBehinderung"></a>3.7. [Optional] Property Befreiung von der Ausweispflicht > ermessensgrundlageBefreiung > dauerhafteBehinderung</strong>  

</summary>
<blockquote>

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Same definition as**    | [betreuer](#ermessensgrundlageBefreiung_handlungsunfaehigkeit_betreuer)     |

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="zustimmungOptionaleAngaben"></a>4. [Required] Property Befreiung von der Ausweispflicht > zustimmungOptionaleAngaben</strong>  

</summary>
<blockquote>

**Title:** Datenschutzhinweis DSGVO

|                |                                          |
| -------------- | ---------------------------------------- |
| **Type**       | `boolean`                                |
| **Defined in** | #/definitions/zustimmungOptionaleAngaben |

**Description:** Basiert auf FIM-Baustein F17005454. Zustimmung der Verwendung optional gemachter Angaben.

</blockquote>
</details>

----------------------------------------------------------------------------------------------------------------------------
Generated using [json-schema-for-humans](https://github.com/coveooss/json-schema-for-humans) on 2024-12-04 at 13:39:49 +0100