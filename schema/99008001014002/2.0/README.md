# Meldung des Verlustes oder Diebstahles eines Ausweisdokuments

**Title:** Meldung des Verlustes oder Diebstahles eines Ausweisdokuments

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="handelndePerson"></a>1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson</strong>  

</summary>
<blockquote>

**Title:** Gesetzlicher Vertreter - Natürliche Person (wenig)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/handelndePerson                                  |

**Description:** Basiert auf dem Baustein G17007412 "Antragstellende Person ". 
Hier werden alle Daten der handelnden Person erfasst. Nur sie kann im Online-Dienst mittels Bund.ID authentifiziert werden. Mit Ausnahme der Kontaktdaten und der Beziehung zur antragstellenden Person werden hier nur Daten übermittelt, die mindestens mit der Vertrauensstufe "niedrig" verifiziert sind. 

<details>
<summary>
<strong> <a name="handelndePerson_beziehungZumAntragsteller"></a>1.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > beziehungZumAntragsteller</strong>  

</summary>
<blockquote>

**Title:** Beziehung zur Antragstellenden Person

|                |                                         |
| -------------- | --------------------------------------- |
| **Type**       | `enum (of string)`                      |
| **Default**    | `0`                                     |
| **Defined in** | #/definitions/beziehungZumAntragsteller |

**Description:** Basiert auf der CodeListe für gesetzliche Vertreter aus DSMeld bzw. XMeld. 
Sie wurde um die meldepflichtige Person (kein gesetzlicher Vertreter) erweitert und etwas vereinfacht. So ist z.B. die geschlechtsspezifische Aufschlüsselung für erziehungsberechtigte Personen nicht mehr zeitgemäß. 
0 - keine (handelnde Person entspricht dem Antragsteller)
2 - Mutter (wird generell für alle erziehungsberechtigten Personen verwendet). Es ist kein weiterer Nachweis erforderlich, da erziehungsberechtigte Personen immer in den Meldedaten gespeichert sind.
3 - Gesetzlicher Vertreter (natürliche Person). Hier sind vor allem Vorsorgebevollmächtigte und gerichtlich bestellte Betreuer gemeint, da für diese Personen ein weiterer Nachweis erforderlich ist.

Must be one of:
* "0"
* "2"
* "5"

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_nachweis"></a>1.2. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > nachweis</strong>  

</summary>
<blockquote>

**Title:** Nachweis der Handlungsbefugnis.

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                    |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |
| **Defined in**            | #/definitions/upload                                                        |

**Description:** Handelt es sich bei der handelnden Person um eine Person, die ggf. nicht im Melderegister eingetragen ist (Vorsorgebevollmächtigter oder gerichtlich bestellter Betreuer), ist ein Nachweis zur Überprüfung der Handlungsvollmacht erforderlich. Dies erfordert immer eine manuelle Prüfung.

<details>
<summary>
<strong> <a name="handelndePerson_nachweis_documentId"></a>1.2.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > nachweis > documentId</strong>  

</summary>
<blockquote>

**Title:** Eindeutige ID des hochgeladenen Dokumentes

|                |                          |
| -------------- | ------------------------ |
| **Type**       | `string`                 |
| **Defined in** | #/definitions/documentId |

**Description:** Eindeutige ID des hochgeladenen Dokumentes zur Identifikation in den Anhängen

**Example:**

```json
"c8859076-76de-41b4-914c-048f84cd9685"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_nachweis_originalName"></a>1.2.2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > nachweis > originalName</strong>  

</summary>
<blockquote>

**Title:** Originaler Dateiname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/originalName |

**Description:** Enthält den Dateinamen, der Ursprünglich von Antragsteller hochgeladenen Datei.

**Examples:**

```json
"nachweis.jpg"
```

```json
"betreuerausweis.pdf"
```

```json
"vorsorgevollmacht.png"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_nachweis_size"></a>1.2.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > nachweis > size</strong>  

</summary>
<blockquote>

**Title:** Größe der Datei

|                |                    |
| -------------- | ------------------ |
| **Type**       | `integer`          |
| **Defined in** | #/definitions/size |

**Description:** Enthält die Größe der Datein in Byte

**Examples:**

```json
1
```

```json
1254678
```

```json
124
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_nachweis_type"></a>1.2.4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > nachweis > type</strong>  

</summary>
<blockquote>

**Title:** Mime Type

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/mimeType |

**Examples:**

```json
"application/pdf"
```

```json
"image/gif"
```

```json
"image/jpeg"
```

```json
"image/png"
```

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_familienname"></a>1.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > familienname</strong>  

</summary>
<blockquote>

**Title:** Familienname

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/familienname |

**Description:** Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.
Basierend auf F60000227.

| Restrictions   |     |
| -------------- | --- |
| **Min length** | 1   |
| **Max length** | 120 |

**Examples:**

```json
"Mustermann"
```

```json
"Musterfrau"
```

```json
"Meier"
```

```json
"Schmidt"
```

```json
"Mohammad"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_vornamen"></a>1.4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > vornamen</strong>  

</summary>
<blockquote>

**Title:** Vornamen

|                |                        |
| -------------- | ---------------------- |
| **Type**       | `string`               |
| **Defined in** | #/definitions/vornamen |

**Description:** Laut BSI TR-03123 soll Vorname <= 80 Zeichen betragen.
Laut PAuswV soll Vorname nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen. Basierend auf F60000228

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 80 |

**Examples:**

```json
"Claudia"
```

```json
"Melanie"
```

```json
"Ali"
```

```json
"Nick David"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift"></a>1.5. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift</strong>  

</summary>
<blockquote>

**Title:** Anschrift Inland oder Ausland mit Frage

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `combining`                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/anschrift                                        |

**Description:** Zu beachten: wird die Anschrift im Kontext der Online-Ausweisfunktion des neuen Personalausweis verwendet, können nur Personen mit Anschriften in Deutschland authentisiert werden (siehe auch https://www.personalausweisportal.de/DE/Buergerinnen-und-Buerger/Online-Ausweisen/Wohnsitz_im_Ausland/wohnsitz_im_ausland_node.html). Bei der Umsetzung müssen somit ausländische Anschriften ausgeschlossen werden.

<blockquote>

| One of(Option)                                                           |
| ------------------------------------------------------------------------ |
| [Anschrift Inland Straßenanschrift](#handelndePerson_anschrift_oneOf_i0) |
| [item 1](#handelndePerson_anschrift_oneOf_i1)                            |

<blockquote>

#### <a name="handelndePerson_anschrift_oneOf_i0"></a>1.5.1. Property `Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift`

**Title:** Anschrift Inland Straßenanschrift

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/anschriftInlandStrasse                           |

**Description:** Basierend auf G60000086. Mit der Abweichung, dass Strasse und Hausnummer zusammengefasst wurden, damit die Kompatiblilität mit den Nutzerkonten gewahrt bleibt.

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_staat"></a>1.5.1.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > staat</strong>  

</summary>
<blockquote>

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"Deutschland"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_strasseHausnummer"></a>1.5.1.2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > strasseHausnummer</strong>  

</summary>
<blockquote>

**Title:** Straße

|                        |                                                                   |
| ---------------------- | ----------------------------------------------------------------- |
| **Type**               | `string`                                                          |
| **Same definition as** | [strasseHausnummer](#handelndePerson_anschrift_strasseHausnummer) |

**Description:** Basierend auf F60000243 kombiniert aber F60000243 mit Hausnummer F60000244, um Kompatibilität mit den Nutzkontne zu erhalten. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 50. Bei XInneres 8 ist die Feldlänge <= 55 Zeichen.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_plz"></a>1.5.1.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > plz</strong>  

</summary>
<blockquote>

**Title:** Postleitzahl

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [plz](#handelndePerson_anschrift_plz) |

**Description:** Basierend auf F60000246

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_ort"></a>1.5.1.4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > ort</strong>  

</summary>
<blockquote>

**Title:** Ort

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [ort](#handelndePerson_anschrift_ort) |

**Description:** Basierend auf F60000247. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.
Laut BSI TR-03123 kleiner gleich 105 Zeichen.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i0_adresszusatz"></a>1.5.1.5. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > Anschrift Inland Straßenanschrift > adresszusatz</strong>  

</summary>
<blockquote>

**Title:** Anschrift Zusatzangaben

|                        |                                                         |
| ---------------------- | ------------------------------------------------------- |
| **Type**               | `string`                                                |
| **Same definition as** | [adresszusatz](#handelndePerson_anschrift_adresszusatz) |

**Description:** Basierend auf F60000248.

</blockquote>
</details>

</blockquote>
<blockquote>

#### <a name="handelndePerson_anschrift_oneOf_i1"></a>1.5.2. Property `Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > item 1`

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i1_staat"></a>1.5.2.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > item 1 > staat</strong>  

</summary>
<blockquote>

|                           |                                                                             |
| ------------------------- | --------------------------------------------------------------------------- |
| **Type**                  | `combining`                                                                 |
| **Additional properties** | ![Any type: allowed](https://img.shields.io/badge/Any%20type-allowed-green) |

###### <a name="autogenerated_heading_2"></a>1.5.2.1.1. Must **not** be

|          |         |
| -------- | ------- |
| **Type** | `const` |

Specific value: `"Deutschland"`

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_oneOf_i1_anschriftKomplett"></a>1.5.2.2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > oneOf > item 1 > anschriftKomplett</strong>  

</summary>
<blockquote>

**Title:** Internationale Anschrift

|                        |                                                                   |
| ---------------------- | ----------------------------------------------------------------- |
| **Type**               | `string`                                                          |
| **Same definition as** | [anschriftKomplett](#handelndePerson_anschrift_anschriftKomplett) |

**Description:** Internationaler Adressblock in Abweichung von G60000091 zur kompatibiliät mit den Nutzerkonten

</blockquote>
</details>

</blockquote>

</blockquote>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_staat"></a>1.5.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > staat</strong>  

</summary>
<blockquote>

**Title:** Staat

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/staat |

**Description:** Entspricht dem FIM-Baustein F60000261. Die Werte entsprechen der amtlichen Kurzform des Staatsnamen aus der Codeliste (urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staat)

**Examples:**

```json
"Deutschland"
```

```json
"Frankreich"
```

```json
"Polen"
```

```json
"Vereinigtes Königreich"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_strasseHausnummer"></a>1.5.4. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > strasseHausnummer</strong>  

</summary>
<blockquote>

**Title:** Straße

|                |                                 |
| -------------- | ------------------------------- |
| **Type**       | `string`                        |
| **Defined in** | #/definitions/strasseHausnummer |

**Description:** Basierend auf F60000243 kombiniert aber F60000243 mit Hausnummer F60000244, um Kompatibilität mit den Nutzkontne zu erhalten. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 50. Bei XInneres 8 ist die Feldlänge <= 55 Zeichen.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 66 |

**Examples:**

```json
"Musterstr. 12a"
```

```json
"E 4, 6"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_plz"></a>1.5.5. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > plz</strong>  

</summary>
<blockquote>

**Title:** Postleitzahl

|                |                   |
| -------------- | ----------------- |
| **Type**       | `string`          |
| **Defined in** | #/definitions/plz |

**Description:** Basierend auf F60000246

| Restrictions                      |                                                                                                                                                                                                       |
| --------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 5                                                                                                                                                                                                     |
| **Max length**                    | 5                                                                                                                                                                                                     |
| **Must match regular expression** | ```^([0]{1}[1-9]{1}\|[1-9]{1}[0-9]{1})[0-9]{3}$``` [Test](https://regex101.com/?regex=%5E%28%5B0%5D%7B1%7D%5B1-9%5D%7B1%7D%7C%5B1-9%5D%7B1%7D%5B0-9%5D%7B1%7D%29%5B0-9%5D%7B3%7D%24&testString=10999) |

**Example:**

```json
10999
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_ort"></a>1.5.6. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > ort</strong>  

</summary>
<blockquote>

**Title:** Ort

|                |                   |
| -------------- | ----------------- |
| **Type**       | `string`          |
| **Defined in** | #/definitions/ort |

**Description:** Basierend auf F60000247. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.
Laut BSI TR-03123 kleiner gleich 105 Zeichen.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 50 |

**Examples:**

```json
"Berlin"
```

```json
"Hamburg"
```

```json
"Lüneburg"
```

```json
"Mannheim"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_adresszusatz"></a>1.5.7. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > adresszusatz</strong>  

</summary>
<blockquote>

**Title:** Anschrift Zusatzangaben

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/adressZusatz |

**Description:** Basierend auf F60000248.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 21 |

**Example:**

```json
"Hinterhaus rechts"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_anschrift_anschriftKomplett"></a>1.5.8. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > anschrift > anschriftKomplett</strong>  

</summary>
<blockquote>

**Title:** Internationale Anschrift

|                |                                 |
| -------------- | ------------------------------- |
| **Type**       | `string`                        |
| **Defined in** | #/definitions/anschriftKomplett |

**Description:** Internationaler Adressblock in Abweichung von G60000091 zur kompatibiliät mit den Nutzerkonten

**Examples:**

```json
"ul. Grodzka 20/ 6, 70-560, Szczecin"
```

```json
"10 bis Rue de la Paix, 75002 Paris"
```

```json
"Via Roma, Frazione Santa Maria, 12050 Alba CN"
```

```json
"Calle de Alcalá, 45, 2º D, 28014 Madrid"
```

```json
"Prinsengracht 267A, 1016 GV Amsterdam"
```

```json
"1 St. Stephen's Green, Dublin 2, D02 AF30"
```

```json
"Bahnhofstrasse 1, 8001 Zürich, Geschäftsgebäude 2"
```

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation"></a>1.6. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > kommunikation</strong>  

</summary>
<blockquote>

**Title:** Kommunikation (wenig, ohne De-Mail)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/kommunikation                                    |

**Description:** G60000183. In dieser Datenfeldgruppe wird per Regel gefordert, dass mindestens eine Kommunikationsmöglichkeit angegeben werden muss. Falls es gewünscht ist, dass keine Kommunikationsmöglichkeit angegeben werden braucht, muss die ganze Feldgruppe bei der Verwendung optional gesetzt werden.

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_telefon"></a>1.6.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > kommunikation > telefon</strong>  

</summary>
<blockquote>

**Title:** Telefon

|                |                       |
| -------------- | --------------------- |
| **Type**       | `string`              |
| **Defined in** | #/definitions/telefon |

**Description:** Basierend auf F60000240. Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 23 |

**Examples:**

```json
"+491234342567"
```

```json
"01718765463"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_telefax"></a>1.6.2. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > kommunikation > telefax</strong>  

</summary>
<blockquote>

**Title:** Telefax

|                |                       |
| -------------- | --------------------- |
| **Type**       | `string`              |
| **Defined in** | #/definitions/telefax |

**Description:** Basierened auf F60000241. Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.

| Restrictions   |    |
| -------------- | -- |
| **Min length** | 1  |
| **Max length** | 23 |

**Examples:**

```json
"+4917112345678"
```

```json
"00493012345678"
```

```json
"030123456789"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="handelndePerson_kommunikation_email"></a>1.6.3. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > handelndePerson > kommunikation > email</strong>  

</summary>
<blockquote>

**Title:** E-Mail

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/email |

**Description:** Basiert auf F60000242

| Restrictions                      |                                                                                                                                                                                                                      |
| --------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 6                                                                                                                                                                                                                    |
| **Max length**                    | 254                                                                                                                                                                                                                  |
| **Must match regular expression** | ```^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$``` [Test](https://regex101.com/?regex=%5E%5BA-Za-z0-9._%25%2B-%5D%2B%40%5BA-Za-z0-9.-%5D%2B%5C.%5BA-Za-z%5D%7B1%2C63%7D%24&testString=%22example%40test.de%22) |

**Examples:**

```json
"example@test.de"
```

```json
"beispiel@demo.com"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson"></a>2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > antragstellendePerson</strong>  

</summary>
<blockquote>

**Title:** Antragstellende Person - Natürliche Person (wenig, PAuswG)

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/antragstellendePerson                            |

**Description:** Basiert auf dem Baustein G17007412.
Dies sind die Daten der Person, der das Dokument gehört und in deren Registereintrag die Meldung erfolgen soll.
Wenn die handelnde Person nicht mit dieser Person identisch ist, können die eingegebenen Daten vom Dokument abweichen.

<details>
<summary>
<strong> <a name="antragstellendePerson_vornamen"></a>2.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > antragstellendePerson > vornamen</strong>  

</summary>
<blockquote>

**Title:** Vornamen

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [vornamen](#handelndePerson_vornamen) |

**Description:** Laut BSI TR-03123 soll Vorname <= 80 Zeichen betragen.
Laut PAuswV soll Vorname nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen. Basierend auf F60000228

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_familienname"></a>2.2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > antragstellendePerson > familienname</strong>  

</summary>
<blockquote>

**Title:** Familienname

|                        |                                               |
| ---------------------- | --------------------------------------------- |
| **Type**               | `string`                                      |
| **Same definition as** | [familienname](#handelndePerson_familienname) |

**Description:** Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.
Basierend auf F60000227.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_geburtsdatum"></a>2.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > antragstellendePerson > geburtsdatum</strong>  

</summary>
<blockquote>

**Title:** Geburtsdatum

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/geburtsdatum |

**Description:** Entspricht der Übermittlung des Geburtsdatum aus den Nutzerkonten. Es dürfen Tag oder Tag und Monat unbekannt sein. Sie werden dann mit Nullen aufgefüllt.

| Restrictions                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **Must match regular expression** | ```^(19\|2[0-1])\d{2}-((0[13578]\|1[02])-31\|(0[1,3-9]\|1[0-2])-(29\|30))\|((19\|2[0-1])(0[48]\|[2468][048]\|[13579][26]))-02-29\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-8])\|2000-02-29\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-00\|(19\|2[0-1])\d{2}-00-00\|0000-00-00\|(19\|2[0-1])\d{2}-(0[1-9]\|1[0-2])-XX\|(19\|2[0-1])\d{2}-XX-XX\|XXXX-XX-XX$``` [Test](https://regex101.com/?regex=%5E%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%28%280%5B13578%5D%7C1%5B02%5D%29-31%7C%280%5B1%2C3-9%5D%7C1%5B0-2%5D%29-%2829%7C30%29%29%7C%28%2819%7C2%5B0-1%5D%29%280%5B48%5D%7C%5B2468%5D%5B048%5D%7C%5B13579%5D%5B26%5D%29%29-02-29%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-%280%5B1-9%5D%7C1%5Cd%7C2%5B0-8%5D%29%7C2000-02-29%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-00%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-00-00%7C0000-00-00%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-%280%5B1-9%5D%7C1%5B0-2%5D%29-XX%7C%2819%7C2%5B0-1%5D%29%5Cd%7B2%7D-XX-XX%7CXXXX-XX-XX%24&testString=%222010-00-00%22) |

**Examples:**

```json
"2010-00-00"
```

```json
"2010-XX-XX"
```

```json
"2010-01-00"
```

```json
"2010-01-XX"
```

```json
"2010-01-31"
```

```json
"2010-01-31"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="antragstellendePerson_anschrift"></a>2.4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > antragstellendePerson > anschrift</strong>  

</summary>
<blockquote>

**Title:** Anschrift Inland oder Ausland mit Frage

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `combining`                                                    |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Same definition as**    | [anschrift](#handelndePerson_anschrift)                        |

**Description:** Zu beachten: wird die Anschrift im Kontext der Online-Ausweisfunktion des neuen Personalausweis verwendet, können nur Personen mit Anschriften in Deutschland authentisiert werden (siehe auch https://www.personalausweisportal.de/DE/Buergerinnen-und-Buerger/Online-Ausweisen/Wohnsitz_im_Ausland/wohnsitz_im_ausland_node.html). Bei der Umsetzung müssen somit ausländische Anschriften ausgeschlossen werden.

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument"></a>3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument</strong>  

</summary>
<blockquote>

**Title:** Angaben Ausweisdokument

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/dokument                                         |

**Description:** Basiert auf G17007413.
Hier werden alle dokumentenspezifischen Daten erfasst. Der hier aufgeführte Status ist der Soll-Status. Er definiert die genaue Durchführung.

<details>
<summary>
<strong> <a name="dokument_typ"></a>3.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > typ</strong>  

</summary>
<blockquote>

**Title:** Art des Dokuments

|                |                             |
| -------------- | --------------------------- |
| **Type**       | `enum (of string)`          |
| **Defined in** | #/definitions/dokumententyp |

**Description:** | Code | Dokument |
|---|---|
| 01 | Deutscher Reisepass |
| 03 | Deutscher Kinderreisepass |
| 09 | Personalausweis |
| 10 | Vorläufiger Personalausweis |
| 11 | Deutscher vorläufiger Reisepass |
| 15 | eID-Karte |

Nach Codeliste xmeld:Code.Pass.und.Ausweisdokumente

Must be one of:
* "reisepass"
* "personalausweis"
* "kinderreisepass"
* "eIdKarte"
* "vorlaufigerReisepass"
* "vorlaufigerPersonalausweis"

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument_status"></a>3.2. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > status</strong>  

</summary>
<blockquote>

**Title:** Status des Dokuments (PAuswG)

|                |                      |
| -------------- | -------------------- |
| **Type**       | `enum (of string)`   |
| **Defined in** | #/definitions/status |

**Description:** Hier wird der Status des Dokuments direkt übergeben. Der Status definiert gleichzeitig die Ausführung des Dienstes, wobei "gestohlen" der " Meldung eines Diebstahls des Personalausweises" und "verloren" der " Meldung eines Verlustes des Personalausweises" entspricht. 
Grundlagen sind hier das PAuswG und
Grundlage ist die folgende Codeliste:
urn:xoev-de:xpassausweis:codeliste:mitzug.dokumentstatus
Es werden jedoch nur die beiden Status "04" "verloren" und "10" "gestohlen" verwendet.

Must be one of:
* "gestohlen"
* "verloren"

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument_seriennummer"></a>3.3. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > seriennummer</strong>  

</summary>
<blockquote>

**Title:** Seriennummer

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/seriennummer |

| Restrictions                      |                                                                                                                                                                                                                                   |
| --------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 9                                                                                                                                                                                                                                 |
| **Max length**                    | 10                                                                                                                                                                                                                                |
| **Must match regular expression** | ```([A-Z]{1}[ ]{1}[0-9]{8})\|([CFGHJKLMNPRTVWXYZ0-9]{9,10})``` [Test](https://regex101.com/?regex=%28%5BA-Z%5D%7B1%7D%5B+%5D%7B1%7D%5B0-9%5D%7B8%7D%29%7C%28%5BCFGHJKLMNPRTVWXYZ0-9%5D%7B9%2C10%7D%29&testString=%22L30F30CLN%22) |

**Examples:**

```json
"L30F30CLN"
```

```json
"A 12345678"
```

```json
"B 87654321"
```

```json
"C 23456789"
```

```json
"D 98765432"
```

```json
"E 34567890"
```

```json
"CFGHJKLMN"
```

```json
"PRTVWXYZ1"
```

```json
"2CFGHJKLM"
```

```json
"NPRTVWXYZ"
```

```json
"WXYZ01234"
```

```json
"CFGHJKLMNP"
```

```json
"1234567890"
```

```json
"RTVWXYZ012"
```

```json
"34567JKLMN"
```

```json
"PRTVWXYZ12"
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument_tagDerAusstellung"></a>3.4. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > tagDerAusstellung</strong>  

</summary>
<blockquote>

**Title:** Ausstelldatum

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/datum |

**Description:** Entspricht dem FIM-Baustein F60000030 "Datum".

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument_letzterTagDerGueltigkeit"></a>3.5. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > letzterTagDerGueltigkeit</strong>  

</summary>
<blockquote>

**Title:** Ablaufdatum

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/datum |

**Description:** Entspricht dem FIM-Baustein F60000030 "Datum".

</blockquote>
</details>

<details>
<summary>
<strong> <a name="dokument_ausstellendeBehoerde"></a>3.6. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > dokument > ausstellendeBehoerde</strong>  

</summary>
<blockquote>

**Title:** Ausstellende Behörde

|                |                                    |
| -------------- | ---------------------------------- |
| **Type**       | `string`                           |
| **Defined in** | #/definitions/ausstellendeBehoerde |

**Description:** Feldlänge orientiert sich an Behoerdenname.Kurzbezeichnung Tabelle 9 BSI TR-03123 Version 1.5.1

| Restrictions                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| --------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Min length**                    | 1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| **Max length**                    | 84                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| **Must match regular expression** | ```([	-
]\|
\|[ -~]\|[ -¬]\|[®-ž]\|[Ƈ-ƈ]\|Ə\|Ɨ\|[Ơ-ơ]\|[Ư-ư]\|Ʒ\|[Ǎ-ǜ]\|[Ǟ-ǟ]\|[Ǣ-ǰ]\|[Ǵ-ǵ]\|[Ǹ-ǿ]\|[Ȓ-ȓ]\|[Ș-ț]\|[Ȟ-ȟ]\|[ȧ-ȳ]\|ə\|ɨ\|ʒ\|[ʹ-ʺ]\|[ʾ-ʿ]\|ˈ\|ˌ\|[Ḃ-ḃ]\|[Ḇ-ḇ]\|[Ḋ-ḑ]\|[Ḝ-ḫ]\|[ḯ-ḷ]\|[Ḻ-ḻ]\|[Ṁ-ṉ]\|[Ṓ-ṛ]\|[Ṟ-ṣ]\|[Ṫ-ṯ]\|[Ẁ-ẇ]\|[Ẍ-ẗ]\|ẞ\|[Ạ-ỹ]\|’\|‡\|€\|A̋\|C(̀\|̄\|̆\|̈\|̕\|̣\|̦\|̨̆)\|D̂\|F(̀\|̄)\|G̀\|H(̄\|̦\|̱)\|J(́\|̌)\|K(̀\|̂\|̄\|̇\|̕\|̛\|̦\|͟H\|͟h)\|L(̂\|̥\|̥̄\|̦)\|M(̀\|̂\|̆\|̐)\|N(̂\|̄\|̆\|̦)\|P(̀\|̄\|̕\|̣)\|R(̆\|̥\|̥̄)\|S(̀\|̄\|̛̄\|̱)\|T(̀\|̄\|̈\|̕\|̛)\|U̇\|Z(̀\|̄\|̆\|̈\|̧)\|a̋\|c(̀\|̄\|̆\|̈\|̕\|̣\|̦\|̨̆)\|d̂\|f(̀\|̄)\|g̀\|h(̄\|̦)\|j́\|k(̀\|̂\|̄\|̇\|̕\|̛\|̦\|͟h)\|l(̂\|̥\|̥̄\|̦)\|m(̀\|̂\|̆\|̐)\|n(̂\|̄\|̆\|̦)\|p(̀\|̄\|̕\|̣)\|r(̆\|̥\|̥̄)\|s(̀\|̄\|̛̄\|̱)\|t(̀\|̄\|̕\|̛)\|u̇\|z(̀\|̄\|̆\|̈\|̧)\|Ç̆\|Û̄\|ç̆\|û̄\|ÿ́\|Č(̕\|̣)\|č(̕\|̣)\|Ī́\|ī́\|Ž(̦\|̧)\|ž(̦\|̧)\|Ḳ̄\|ḳ̄\|Ṣ̄\|ṣ̄\|Ṭ̄\|ṭ̄\|Ạ̈\|ạ̈\|Ọ̈\|ọ̈\|Ụ(̄\|̈)\|ụ(̄\|̈))*``` [Test](https://regex101.com/?regex=%28%5B%09-%0A%5D%7C%0D%7C%5B+-~%5D%7C%5B%C2%A0-%C2%AC%5D%7C%5B%C2%AE-%C5%BE%5D%7C%5B%C6%87-%C6%88%5D%7C%C6%8F%7C%C6%97%7C%5B%C6%A0-%C6%A1%5D%7C%5B%C6%AF-%C6%B0%5D%7C%C6%B7%7C%5B%C7%8D-%C7%9C%5D%7C%5B%C7%9E-%C7%9F%5D%7C%5B%C7%A2-%C7%B0%5D%7C%5B%C7%B4-%C7%B5%5D%7C%5B%C7%B8-%C7%BF%5D%7C%5B%C8%92-%C8%93%5D%7C%5B%C8%98-%C8%9B%5D%7C%5B%C8%9E-%C8%9F%5D%7C%5B%C8%A7-%C8%B3%5D%7C%C9%99%7C%C9%A8%7C%CA%92%7C%5B%CA%B9-%CA%BA%5D%7C%5B%CA%BE-%CA%BF%5D%7C%CB%88%7C%CB%8C%7C%5B%E1%B8%82-%E1%B8%83%5D%7C%5B%E1%B8%86-%E1%B8%87%5D%7C%5B%E1%B8%8A-%E1%B8%91%5D%7C%5B%E1%B8%9C-%E1%B8%AB%5D%7C%5B%E1%B8%AF-%E1%B8%B7%5D%7C%5B%E1%B8%BA-%E1%B8%BB%5D%7C%5B%E1%B9%80-%E1%B9%89%5D%7C%5B%E1%B9%92-%E1%B9%9B%5D%7C%5B%E1%B9%9E-%E1%B9%A3%5D%7C%5B%E1%B9%AA-%E1%B9%AF%5D%7C%5B%E1%BA%80-%E1%BA%87%5D%7C%5B%E1%BA%8C-%E1%BA%97%5D%7C%E1%BA%9E%7C%5B%E1%BA%A0-%E1%BB%B9%5D%7C%E2%80%99%7C%E2%80%A1%7C%E2%82%AC%7CA%CC%8B%7CC%28%CC%80%7C%CC%84%7C%CC%86%7C%CC%88%7C%CC%95%7C%CC%A3%7C%CC%A6%7C%CC%A8%CC%86%29%7CD%CC%82%7CF%28%CC%80%7C%CC%84%29%7CG%CC%80%7CH%28%CC%84%7C%CC%A6%7C%CC%B1%29%7CJ%28%CC%81%7C%CC%8C%29%7CK%28%CC%80%7C%CC%82%7C%CC%84%7C%CC%87%7C%CC%95%7C%CC%9B%7C%CC%A6%7C%CD%9FH%7C%CD%9Fh%29%7CL%28%CC%82%7C%CC%A5%7C%CC%A5%CC%84%7C%CC%A6%29%7CM%28%CC%80%7C%CC%82%7C%CC%86%7C%CC%90%29%7CN%28%CC%82%7C%CC%84%7C%CC%86%7C%CC%A6%29%7CP%28%CC%80%7C%CC%84%7C%CC%95%7C%CC%A3%29%7CR%28%CC%86%7C%CC%A5%7C%CC%A5%CC%84%29%7CS%28%CC%80%7C%CC%84%7C%CC%9B%CC%84%7C%CC%B1%29%7CT%28%CC%80%7C%CC%84%7C%CC%88%7C%CC%95%7C%CC%9B%29%7CU%CC%87%7CZ%28%CC%80%7C%CC%84%7C%CC%86%7C%CC%88%7C%CC%A7%29%7Ca%CC%8B%7Cc%28%CC%80%7C%CC%84%7C%CC%86%7C%CC%88%7C%CC%95%7C%CC%A3%7C%CC%A6%7C%CC%A8%CC%86%29%7Cd%CC%82%7Cf%28%CC%80%7C%CC%84%29%7Cg%CC%80%7Ch%28%CC%84%7C%CC%A6%29%7Cj%CC%81%7Ck%28%CC%80%7C%CC%82%7C%CC%84%7C%CC%87%7C%CC%95%7C%CC%9B%7C%CC%A6%7C%CD%9Fh%29%7Cl%28%CC%82%7C%CC%A5%7C%CC%A5%CC%84%7C%CC%A6%29%7Cm%28%CC%80%7C%CC%82%7C%CC%86%7C%CC%90%29%7Cn%28%CC%82%7C%CC%84%7C%CC%86%7C%CC%A6%29%7Cp%28%CC%80%7C%CC%84%7C%CC%95%7C%CC%A3%29%7Cr%28%CC%86%7C%CC%A5%7C%CC%A5%CC%84%29%7Cs%28%CC%80%7C%CC%84%7C%CC%9B%CC%84%7C%CC%B1%29%7Ct%28%CC%80%7C%CC%84%7C%CC%95%7C%CC%9B%29%7Cu%CC%87%7Cz%28%CC%80%7C%CC%84%7C%CC%86%7C%CC%88%7C%CC%A7%29%7C%C3%87%CC%86%7C%C3%9B%CC%84%7C%C3%A7%CC%86%7C%C3%BB%CC%84%7C%C3%BF%CC%81%7C%C4%8C%28%CC%95%7C%CC%A3%29%7C%C4%8D%28%CC%95%7C%CC%A3%29%7C%C4%AA%CC%81%7C%C4%AB%CC%81%7C%C5%BD%28%CC%A6%7C%CC%A7%29%7C%C5%BE%28%CC%A6%7C%CC%A7%29%7C%E1%B8%B2%CC%84%7C%E1%B8%B3%CC%84%7C%E1%B9%A2%CC%84%7C%E1%B9%A3%CC%84%7C%E1%B9%AC%CC%84%7C%E1%B9%AD%CC%84%7C%E1%BA%A0%CC%88%7C%E1%BA%A1%CC%88%7C%E1%BB%8C%CC%88%7C%E1%BB%8D%CC%88%7C%E1%BB%A4%28%CC%84%7C%CC%88%29%7C%E1%BB%A5%28%CC%84%7C%CC%88%29%29%2A) |

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung"></a>4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung</strong>  

</summary>
<blockquote>

**Title:** Angaben zur Meldung des Verlustes bzw. Diebstahls

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/meldung                                          |

**Description:** Es werden alle Daten verfasst, die ggf. in einer Nachricht an eine weitere Behörde benötigt werden. 

<details>
<summary>
<strong> <a name="meldung_anfang"></a>4.1. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > anfang</strong>  

</summary>
<blockquote>

**Title:** Anfang

|                |                      |
| -------------- | -------------------- |
| **Type**       | `string`             |
| **Defined in** | #/definitions/anfang |

**Description:** Entspricht dem FIM-Baustein F60000048 Anfang.
Es wird das Anfangsdatum eines Zeitraumes definiert.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung_ende"></a>4.2. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > ende</strong>  

</summary>
<blockquote>

**Title:** Ende

|                |                    |
| -------------- | ------------------ |
| **Type**       | `string`           |
| **Defined in** | #/definitions/ende |

**Description:** Entspricht dem FIM Baustein F60000049.
Es wird das Ende eines Zeitraums definiert.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung_ort"></a>4.3. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > ort</strong>  

</summary>
<blockquote>

**Title:** Ort

|                        |                                       |
| ---------------------- | ------------------------------------- |
| **Type**               | `string`                              |
| **Same definition as** | [ort](#handelndePerson_anschrift_ort) |

**Description:** Basierend auf F60000247. Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.
Laut BSI TR-03123 kleiner gleich 105 Zeichen.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung_umstaende"></a>4.4. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > umstaende</strong>  

</summary>
<blockquote>

**Title:** Umstände des Verlusts/Diebstahls

|                |                         |
| -------------- | ----------------------- |
| **Type**       | `string`                |
| **Defined in** | #/definitions/umstaende |

**Description:** Basiert auf FIM-Baustein F17012135.
Dient zur Übermittlung an die jeweiligen Polizeidienststellen.

| Restrictions   |      |
| -------------- | ---- |
| **Max length** | 1500 |

**Example:**

```json
"Der Ausweis wurde in der U-Bahn in Richtung Harburg zusammen mit dem gesamten Geldbeutel verloren."
```

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung_anzeige"></a>4.5. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > anzeige</strong>  

</summary>
<blockquote>

**Title:** Anzeige

|                           |                                                                |
| ------------------------- | -------------------------------------------------------------- |
| **Type**                  | `object`                                                       |
| **Additional properties** | ![Not allowed](https://img.shields.io/badge/Not%20allowed-red) |
| **Defined in**            | #/definitions/anzeige                                          |

**Description:** Basiert auf G17007418.
Angaben zur polizeilichen Anzeige eines Verlusts oder Diebstahls.

<details>
<summary>
<strong> <a name="meldung_anzeige_datumDerAnzeige"></a>4.5.1. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > anzeige > datumDerAnzeige</strong>  

</summary>
<blockquote>

**Title:** Datum der Anzeige

|                |                     |
| -------------- | ------------------- |
| **Type**       | `string`            |
| **Defined in** | #/definitions/datum |

**Description:** Entspricht dem FIM-Baustein F60000030 "Datum".
Hier wird der Tag der Anzeige erfasst.

</blockquote>
</details>

<details>
<summary>
<strong> <a name="meldung_anzeige_dienststelle"></a>4.5.2. [Optional] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > meldung > anzeige > dienststelle</strong>  

</summary>
<blockquote>

**Title:** Dienststelle (PAuswG)

|                |                            |
| -------------- | -------------------------- |
| **Type**       | `string`                   |
| **Defined in** | #/definitions/dienststelle |

**Description:** Basiert auf F60000292 "Ausstellende Behörde" und erfasst die Polizeidienststelle, bei der die Anzeige erstattet wurde.

**Examples:**

```json
"Polizeikommissariat 15, Davidwache"
```

```json
"Polizeikommissariat 14"
```

</blockquote>
</details>

</blockquote>
</details>

</blockquote>
</details>

<details>
<summary>
<strong> <a name="zustimmungOptionaleAngaben"></a>5. [Required] Property Meldung des Verlustes oder Diebstahles eines Ausweisdokuments > zustimmungOptionaleAngaben</strong>  

</summary>
<blockquote>

**Title:** Datenschutzhinweis DSGVO

|                |                                          |
| -------------- | ---------------------------------------- |
| **Type**       | `boolean`                                |
| **Defined in** | #/definitions/zustimmungOptionaleAngaben |

**Description:** Basiert auf FIM-Baustein F17005454. Zustimmung der Verwendung optional gemachter Angaben.

</blockquote>
</details>

----------------------------------------------------------------------------------------------------------------------------
Generated using [json-schema-for-humans](https://github.com/coveooss/json-schema-for-humans) on 2024-12-04 at 13:07:47 +0100