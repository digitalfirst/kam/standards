{
    "type": "object",
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://schema.fitko.de/fim/s17000796_0.3.schema.json",
    "title": "OZG-RDS_Ausweispflicht Befreiung",
    "$defs": {
        "F17005454": {
            "title": "Datenschutzhinweis DSGVO",
            "type": "boolean"
        },
        "F17005455": {
            "title": "Zustimmung zu einem digitalen Bescheid",
            "type": "boolean"
        },
        "G17007424": {
            "title": "Handelnde Person (PAuswG)",
            "type": "object",
            "properties": {
                "F17012146": {
                    "$ref": "#/$defs/F17012146"
                },
                "G60000224": {
                    "$ref": "#/$defs/G60000224"
                },
                "G17007419": {
                    "$ref": "#/$defs/G17007419"
                }
            },
            "required": [
                "F17012146"
            ]
        },
        "F17012146": {
            "title": "Antragstellende Person = Handelnde Person? (PAuswG)",
            "type": "boolean"
        },
        "G60000224": {
            "title": "Gesetzlicher Vertreter - Natürliche Person (wenig)",
            "description": "Diese Gruppe weicht vom Kerndatenmodell leicht ab, um einen höheren Praxisbezug zu schaffen. Die Abweichung findet sich in der Nutzung Anschriftsgruppe und der Kommunikationsgruppe, hier werden stattdessen die Gruppe G60000088 'Anschrift Inland', sowie G60000183 'Kommunikation (wenig, ohne De-Mail)' verwendet Dem Kerndatenmodell entspricht die Gruppe G60000112 'Gesetzlicher Vertreter - Natürliche Person (abstrakt)'.",
            "type": "object",
            "properties": {
                "F60000227": {
                    "$ref": "#/$defs/F60000227"
                },
                "F60000230": {
                    "$ref": "#/$defs/F60000230"
                },
                "F60000228": {
                    "$ref": "#/$defs/F60000228"
                },
                "F60000229": {
                    "$ref": "#/$defs/F60000229"
                },
                "G60000083": {
                    "$ref": "#/$defs/G60000083"
                },
                "G60000093": {
                    "$ref": "#/$defs/G60000093"
                },
                "G60000183": {
                    "$ref": "#/$defs/G60000183"
                }
            },
            "required": [
                "F60000227",
                "F60000228",
                "G60000083",
                "G60000093",
                "G60000183"
            ]
        },
        "F60000227": {
            "title": "Familienname",
            "description": "Laut BSI TR-03123 soll die Gesamtlänge für Familienname, Titel und Geburtsname nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 120
        },
        "F60000230": {
            "title": "Geburtsname",
            "description": "Laut BSI TR-03123 soll Geburtsname <= 75 Zeichen betragen. Die Gesamtlänge für Familienname, Titel und Geburtsname soll nicht mehr als 120 Zeichen betragen. Laut PAuswV soll Name (Familienname und Geburtsname) nicht mehr als 2*26 = 52 Zeichen bzw. 3*40 = 120 Zeichen betragen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 75
        },
        "F60000228": {
            "title": "Vornamen",
            "description": "Laut BSI TR-03123 soll Vorname <= 80 Zeichen betragen.\r\nLaut PAuswV soll Vorname nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 80
        },
        "F60000229": {
            "title": "Doktorgrade",
            "description": "Die Feldlänge ist laut BSI TR-03123 (hier Titel genannt) maximal 120 (Familienname + Titel + Geburtsname). \r\nDurch die Vielzahl von Kombinationsmöglichkeiten, falls mehrere Doktortitel vorhanden sind, ist es nicht sinnvoll, eine Codeliste zu hinterlegen.",
            "type": "string",
            "minLength": 3,
            "maxLength": 119
        },
        "G60000083": {
            "title": "Geburtsdatum (teilbekannt)",
            "type": "object",
            "properties": {
                "F60000231": {
                    "$ref": "#/$defs/F60000231"
                },
                "F60000232": {
                    "$ref": "#/$defs/F60000232"
                },
                "F60000233": {
                    "$ref": "#/$defs/F60000233"
                }
            },
            "required": [
                "F60000233"
            ]
        },
        "F60000231": {
            "title": "Tag (ohne Monat und Jahr)",
            "type": "integer",
            "minimum": 1,
            "maximum": 31
        },
        "F60000232": {
            "title": "Monat",
            "type": "integer",
            "minimum": 1,
            "maximum": 12
        },
        "F60000233": {
            "title": "Jahr",
            "type": "integer",
            "minimum": 1850,
            "maximum": 2080
        },
        "G60000093": {
            "title": "Anschrift Inland oder Ausland mit Frage",
            "description": "Zu beachten: wird die Anschrift im Kontext der Online-Ausweisfunktion des neuen Personalausweis verwendet, können nur Personen mit Anschriften in Deutschland authentisiert werden (siehe auch https://www.personalausweisportal.de/DE/Buergerinnen-und-Buerger/Online-Ausweisen/Wohnsitz_im_Ausland/wohnsitz_im_ausland_node.html). Bei der Umsetzung müssen somit ausländische Anschriften ausgeschlossen werden.",
            "type": "object",
            "properties": {
                "F60000263": {
                    "$ref": "#/$defs/F60000263"
                },
                "G60000088": {
                    "$ref": "#/$defs/G60000088"
                },
                "G60000091": {
                    "$ref": "#/$defs/G60000091"
                }
            },
            "required": [
                "F60000263"
            ]
        },
        "F60000263": {
            "title": "Abfrage Anschrift Inland oder Ausland",
            "type": "string",
            "enum": [
                "001",
                "002"
            ]
        },
        "G60000088": {
            "title": "Anschrift Inland",
            "type": "object",
            "properties": {
                "G60000086": {
                    "$ref": "#/$defs/G60000086"
                },
                "G60000087": {
                    "$ref": "#/$defs/G60000087"
                }
            }
        },
        "G60000086": {
            "title": "Anschrift Inland Straßenanschrift",
            "type": "object",
            "properties": {
                "F60000243": {
                    "$ref": "#/$defs/F60000243"
                },
                "F60000244": {
                    "$ref": "#/$defs/F60000244"
                },
                "F60000246": {
                    "$ref": "#/$defs/F60000246"
                },
                "F60000247": {
                    "$ref": "#/$defs/F60000247"
                },
                "F60000248": {
                    "$ref": "#/$defs/F60000248"
                }
            },
            "required": [
                "F60000243",
                "F60000246",
                "F60000247"
            ]
        },
        "F60000243": {
            "title": "Straße",
            "description": "Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 50. Bei XInneres 8 ist die Feldlänge <= 55 Zeichen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 55
        },
        "F60000244": {
            "title": "Hausnummer",
            "description": "Die Modellierung aus XInneres wurde nicht 1:1 übernommen, um die Komplexität für die Ausfüllenden nicht zu hoch zu setzen. Es wurde darauf verzichtet für das Ende von Hausnummernbereichen eigene Felder zu modellieren. Daher ist die Feldlänge hier heraufgesetzt und eine entsprechende Beschreibung wurde eingefügt. Die Feldlänge setzt sich aus der maximalen Feldlänge zweier Hausnummern (jeweils 4), zweier Buchstaben (jeweils 1) und eines Zeichen (-) zusammen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 11
        },
        "F60000246": {
            "title": "Postleitzahl",
            "type": "string",
            "minLength": 5,
            "maxLength": 5,
            "pattern": "^([0]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{3}$"
        },
        "F60000247": {
            "title": "Ort",
            "description": "Kompatibilität zu EPA in TR XhD v 1.4 sollte Feldlänge min. 44. Laut PAuswV 2*25 = 50 Zeichen. Laut Xinneres.Meldeanschrift.Wohnort Version 8 = 40 Zeichen.\r\nLaut BSI TR-03123 kleiner gleich 105 Zeichen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "F60000248": {
            "title": "Anschrift Zusatzangaben",
            "type": "string",
            "minLength": 1,
            "maxLength": 21
        },
        "G60000087": {
            "title": "Anschrift Inland Postfachanschrift",
            "type": "object",
            "properties": {
                "F60000249": {
                    "$ref": "#/$defs/F60000249"
                },
                "F60000246": {
                    "$ref": "#/$defs/F60000246"
                },
                "F60000247": {
                    "$ref": "#/$defs/F60000247"
                }
            },
            "required": [
                "F60000246",
                "F60000247"
            ]
        },
        "F60000249": {
            "title": "Postfach",
            "description": "Die Zeichenlänge des Postfachs orientiert sich derzeit an der Länge einer Adresszeile auf einem Brief. Eine Schärfung/Überprüfung steht noch aus.",
            "type": "string",
            "minLength": 1,
            "maxLength": 21
        },
        "G60000091": {
            "title": "Anschrift Ausland",
            "description": "Da es um eine Auslandsanschrift handelt, sollte in der Codeliste des Feldes F60000261 Staat, wenn möglich Deutschland ausgeblendet werden.",
            "type": "object",
            "properties": {
                "F60000261": {
                    "$ref": "#/$defs/F60000261"
                },
                "G60000092": {
                    "$ref": "#/$defs/G60000092"
                }
            },
            "required": [
                "F60000261",
                "G60000092"
            ]
        },
        "F60000261": {
            "title": "Staat",
            "type": "string",
            "enum": [
                "000",
                "121",
                "122",
                "123",
                "124",
                "125",
                "126",
                "127",
                "128",
                "129",
                "130",
                "131",
                "134",
                "135",
                "136",
                "137",
                "139",
                "140",
                "141",
                "142",
                "143",
                "144",
                "145",
                "146",
                "147",
                "148",
                "149",
                "150",
                "151",
                "152",
                "153",
                "154",
                "155",
                "156",
                "157",
                "158",
                "160",
                "161",
                "163",
                "164",
                "165",
                "166",
                "167",
                "168",
                "169",
                "170",
                "181",
                "221",
                "223",
                "224",
                "225",
                "226",
                "227",
                "229",
                "230",
                "231",
                "232",
                "233",
                "236",
                "237",
                "238",
                "239",
                "242",
                "243",
                "244",
                "245",
                "246",
                "247",
                "248",
                "249",
                "251",
                "252",
                "253",
                "254",
                "255",
                "256",
                "257",
                "258",
                "259",
                "261",
                "262",
                "263",
                "265",
                "267",
                "268",
                "269",
                "271",
                "272",
                "273",
                "274",
                "277",
                "278",
                "281",
                "282",
                "283",
                "284",
                "285",
                "286",
                "287",
                "289",
                "291",
                "320",
                "322",
                "323",
                "324",
                "326",
                "327",
                "328",
                "330",
                "332",
                "333",
                "334",
                "335",
                "336",
                "337",
                "340",
                "345",
                "346",
                "347",
                "348",
                "349",
                "351",
                "353",
                "354",
                "355",
                "357",
                "359",
                "361",
                "364",
                "365",
                "366",
                "367",
                "368",
                "369",
                "370",
                "371",
                "421",
                "422",
                "423",
                "424",
                "425",
                "426",
                "427",
                "429",
                "430",
                "431",
                "432",
                "434",
                "436",
                "437",
                "438",
                "439",
                "441",
                "442",
                "444",
                "445",
                "446",
                "447",
                "448",
                "449",
                "450",
                "451",
                "454",
                "456",
                "457",
                "458",
                "460",
                "461",
                "462",
                "467",
                "469",
                "470",
                "471",
                "472",
                "474",
                "475",
                "476",
                "477",
                "479",
                "482",
                "483",
                "523",
                "524",
                "526",
                "527",
                "530",
                "531",
                "532",
                "533",
                "536",
                "537",
                "538",
                "540",
                "541",
                "543",
                "544",
                "545"
            ]
        },
        "G60000092": {
            "title": "Anschriftzone",
            "type": "object",
            "properties": {
                "F60000262": {
                    "type": "array",
                    "items": {
                        "$ref": "#/$defs/F60000262"
                    },
                    "minItems": 2,
                    "maxItems": 5
                }
            },
            "required": [
                "F60000262"
            ]
        },
        "F60000262": {
            "title": "ZeileAnschrift",
            "type": "string",
            "minLength": 1,
            "maxLength": 35
        },
        "G60000183": {
            "title": "Kommunikation (wenig, ohne De-Mail)",
            "description": "In dieser Datenfeldgruppe wird per Regel gefordert, dass mindestens eine Kommunikationsmöglichkeit angegeben werden muss. Falls es gewünscht ist, dass keine Kommunikationsmöglichkeit angegeben werden braucht, muss die ganze Feldgruppe bei der Verwendung optional gesetzt werden.",
            "type": "object",
            "properties": {
                "F60000240": {
                    "$ref": "#/$defs/F60000240"
                },
                "F60000241": {
                    "$ref": "#/$defs/F60000241"
                },
                "F60000242": {
                    "$ref": "#/$defs/F60000242"
                }
            }
        },
        "F60000240": {
            "title": "Telefon",
            "description": "Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.",
            "type": "string",
            "minLength": 1,
            "maxLength": 23
        },
        "F60000241": {
            "title": "Telefax",
            "description": "Dieses Feld wurde angelehnt an ITU E.123. Eine Prüfung über ein Pattern erfolgt nicht, um den Eingebenden nicht zu überfordern.",
            "type": "string",
            "minLength": 1,
            "maxLength": 23
        },
        "F60000242": {
            "title": "E-Mail",
            "type": "string",
            "minLength": 6,
            "maxLength": 254,
            "pattern": "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$"
        },
        "G17007419": {
            "title": "Antragsstellende Person (PAuswG)",
            "type": "object",
            "properties": {
                "F60000228": {
                    "$ref": "#/$defs/F60000228"
                },
                "F60000227": {
                    "$ref": "#/$defs/F60000227"
                },
                "G60000083": {
                    "$ref": "#/$defs/G60000083"
                },
                "F60000234": {
                    "$ref": "#/$defs/F60000234"
                },
                "G60000088": {
                    "$ref": "#/$defs/G60000088"
                }
            },
            "required": [
                "F60000228",
                "F60000227",
                "G60000083",
                "F60000234",
                "G60000088"
            ]
        },
        "F60000234": {
            "title": "Geburtsort",
            "description": "Laut BSI TR-03123 soll Geburtsort <= 80 Zeichen betragen. Laut PAuswV soll Geburtsort nicht mehr als 26 Zeichen bzw. 2*40 = 80 Zeichen betragen.",
            "type": "string",
            "minLength": 1,
            "maxLength": 80
        },
        "G17007420": {
            "title": "Ermessungsgrundlage Befreiung (PAuswG)",
            "type": "object",
            "properties": {
                "F17012136": {
                    "$ref": "#/$defs/F17012136"
                },
                "G17007421": {
                    "$ref": "#/$defs/G17007421"
                },
                "G17007422": {
                    "$ref": "#/$defs/G17007422"
                },
                "G17007423": {
                    "$ref": "#/$defs/G17007423"
                }
            },
            "required": [
                "F17012136"
            ]
        },
        "F17012136": {
            "title": "Ermessungsgrundlage Befreiung auswählen (PAuswG)",
            "type": "string",
            "enum": [
                "001",
                "002",
                "003"
            ]
        },
        "G17007421": {
            "title": "Handlungsunfähigkeit nachweisen (PAuswG)",
            "type": "object",
            "properties": {
                "F17012137": {
                    "$ref": "#/$defs/F17012137"
                },
                "F17012138": {
                    "$ref": "#/$defs/F17012138"
                }
            }
        },
        "F17012137": {
            "title": "Betreuerausweis hochladen (PAuswG)",
            "type": "string"
        },
        "F17012138": {
            "title": "Vorsorgevollmacht hochladen (PAuswG)",
            "type": "string"
        },
        "G17007422": {
            "title": "Dauerhaft in Pfege-/Krankenhaus untergebracht nachweisen (PAuswG)",
            "type": "object",
            "properties": {
                "F17012140": {
                    "$ref": "#/$defs/F17012140"
                },
                "F17012139": {
                    "$ref": "#/$defs/F17012139"
                },
                "F17012141": {
                    "$ref": "#/$defs/F17012141"
                }
            },
            "required": [
                "F17012140",
                "F17012139"
            ]
        },
        "F17012140": {
            "title": "Anerkanntes Pflegeheim? (PAuswG)",
            "type": "boolean"
        },
        "F17012139": {
            "title": "Aufnahmebestätigung Krankenhaus oder Pflegeheim hochladen (PAuswG)",
            "type": "string"
        },
        "F17012141": {
            "title": "Pflegestufe Nachweis hochladen (PAuswG)",
            "type": "string"
        },
        "G17007423": {
            "title": "Dauerhafte Behinderung nachweisen (PAuswG)",
            "type": "object",
            "properties": {
                "F17012142": {
                    "$ref": "#/$defs/F17012142"
                },
                "F17012143": {
                    "$ref": "#/$defs/F17012143"
                },
                "F17012144": {
                    "$ref": "#/$defs/F17012144"
                }
            }
        },
        "F17012142": {
            "title": "Behinderungsgrad Nachweis hochladen (PAuswG)",
            "type": "string"
        },
        "F17012143": {
            "title": "Bettlägerigkeit Nachweis hochladen (PAuswG)",
            "type": "string"
        },
        "F17012144": {
            "title": "Ärztliche Bescheinigung hochladen (PAuswG)",
            "type": "string"
        }
    },
    "properties": {
        "F17005454": {
            "$ref": "#/$defs/F17005454"
        },
        "F17005455": {
            "$ref": "#/$defs/F17005455"
        },
        "G17007424": {
            "$ref": "#/$defs/G17007424"
        },
        "G17007420": {
            "$ref": "#/$defs/G17007420"
        }
    },
    "required": [
        "F17005454",
        "F17005455",
        "G17007424",
        "G17007420"
    ]
}