# FIM-Stammdaten
Die hier abgelegten Stammdatenschemata entsprechen dem Arbeitsstand zum Ende des Jahres 2023. Seit Mitte Juni 2024 stehen die ersten Versionen der beiden Stammdatenschemata im Gold-Status im FIM-Portal zur Verfügung. Die hier aufgelisteten Links führen direkt auf die Seiten im neuen FIM-Portal, sodass alle notwendigen Informationen direkt abgerufen werden können.

## offizielle Links der FIM-Stammdaten
* [Meldung Personalausweis](https://neu.fimportal.de/schemas/S00000311/1.0)
* [Befreiung Ausweispflicht](https://neu.fimportal.de/schemas/S00000310/1.0)


# FIM-Prozesse
Wir bedauern, Ihnen mitteilen zu müssen, dass die Prozesse noch nicht öffentlich im neuen FIM-Portal verfügbar sind. Die PDFs des letzten Arbeitsstands können jedoch hier gefunden werden.