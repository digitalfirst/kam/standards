# Repository des Programms Digitalfirst der Senatskanzlei Hamburg

## Info
In diesem Repository veröffentlichen wir Infomationen rund um unsere EFA-Onlinedienste, die wir Rahmen des OZG erstellt haben.

### FIM Schema
FIM-Schema werden in Zusammenarbeit mit dem Key Account Management und den FIM-Experten bereitgestellt.

### Standards
Informationen zu Standards sind im [Standard-Repository](https://gitlab.opencode.de/digitalfirst/kam/standards/-/tree/main/docs?ref_type=heads) zu finden.
Ansprechpartner für weitere Standardisierungen ist die Architektur Crew des Programms DigitalFirst.

## Kontakt
Architekur Crew des Programms DigitalFirst:
[Funktionspostfach der Architeken](mailto:d1st-architekten@sk.hamburg.de)

Unterstützung bei der Pflege bzw. Bereitstellung von Schema-Dateien gibt es bei
[Axel Wolters](mailto:axel.wolters@sk.hamburg.de)

Das Key Account Team ist über dieses Funktionspostfach zu erreichen:
[efahamburgkey-account-management@sk.hamburg.de](mailto:efahamburgkey-account-management@sk.hamburg.de)

## Weitere Informationen

### Hosting von JSON Schema
Der Ablageort für JSON Schema hat diese Struktur:

./schema/%LeiKa-Nummer%/%Version%/schema.json

Der Basis-URL-Pfad ist immer:
* für View: https://gitlab.opencode.de/digitalfirst/kam/standards/-/blob/main/
* für Download/Raw: https://gitlab.opencode.de/digitalfirst/kam/standards/-/raw/main/


Beispiele für komplette Pfade zu einem Schema:
* für View: https://gitlab.opencode.de/digitalfirst/kam/standards/-/blob/main/schema/-leikanummer-/1.0/schema.json
* für Download/Raw: https://gitlab.opencode.de/digitalfirst/kam/standards/-/raw/main/schema/-leikanummer-/1.0/schema.json

### Impressum
Programm Digital First
der Hamburger Senatskanzlei

Amt für IT und Digitalisierung
Senat der Freien und Hansestadt Hamburg – Senatskanzlei

Postanschrift: Rathausmarkt 1, 20095 Hamburg
Büroanschrift: Caffamacherreihe 1-3, 20355 Hamburg

